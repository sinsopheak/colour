//
//  HomeTableViewCell.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var colorCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
