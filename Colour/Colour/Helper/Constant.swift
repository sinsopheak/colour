//
//  Constant.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import Foundation

struct Constant {
    
    static let baseURL: String = "http://www.colr.org/json/"
    static let randomColorURL: String = baseURL + "colors/random/"
    static let randomSchemeURL: String = baseURL + "schemes/random/"
    static let colorDetail: String = baseURL + "color/"
    static let searchURL: String = baseURL + "tag/"
    
    static let baseURLConverter: String = "http://www.thecolorapi.com/id?"
    static let convertHexValue: String = baseURLConverter + "hex="
    static let convertRGBValue: String = baseURLConverter + "rgb=rgb"
    
    static let hexString: String = "0123456789abcdefABCDEF"
    static let rgbString: String = "0123456789"
    
    static let testFairyToken: String = "5dbf8fc70ce68db65ffac10c91cf52913e117517"
    
    static let accountState: String = "isSignedIn"
    
}
