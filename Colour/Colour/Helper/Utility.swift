//
//  Utility.swift
//  Colour
//
//  Created by gis on 9/28/18.
//

import Foundation

class Utility {
    static func isSignedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: Constant.accountState)
    }
    
    static func setAccountState(_ isSignedIn: Bool) {
        UserDefaults.standard.set(isSignedIn, forKey: Constant.accountState)
    }
    
    static func getCurrentDateTime() -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone() as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date = NSDate()
        let localDateTime = dateFormatter.string(from: date as Date)
        
        return localDateTime
    }
}
