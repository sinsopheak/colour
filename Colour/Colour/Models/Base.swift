//
//  Base.swift
//  Colour
//
//  Created by gis on 9/25/18.
//

import Foundation

class Base: NSObject {
    var id: Int!
    
    init(_ id: Int!) {
        self.id = id
    }
}
