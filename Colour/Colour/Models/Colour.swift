//
//  Colour.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import Foundation

class Colour: Base {
    
    var hex: String!

    init(_ id: Int!, _ hex: String!) {
        super.init(id)
        self.hex = uppercase(hex)
    }
    
    func uppercase(_ text: String!) -> String {
        return text.uppercased()
    }
}
