//
//  Saved.swift
//  Colour
//
//  Created by gis on 9/28/18.
//

import Foundation

class Saved: NSObject {
    
    var colour: Colour!
    var dateAdded: String!
    var id: String
    
    init(_ id: String!, _ colour: Colour!, _ dateAdded: String!) {
        self.id = id
        self.colour = colour
        self.dateAdded = dateAdded
    }
}
