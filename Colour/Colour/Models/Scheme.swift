//
//  Scheme.swift
//  Colour
//
//  Created by gis on 9/25/18.
//

import Foundation

class Scheme: Base {
    
    var colors: [Colour]!
    var tags: [Tag]!
    
    init(_ id: Int!, _ colors: [Colour]!, _ tags: [Tag]!) {
        super.init(id)
        self.colors = colors
        self.tags = tags
    }
}
