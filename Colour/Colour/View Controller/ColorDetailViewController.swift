//
//  ColorDetailViewController.swift
//  Colour
//
//  Created by gis on 9/25/18.
//

import UIKit
import FirebaseDatabase

private var segueIdentifier = "signFirst"

class ColorDetailViewController: UIViewController {
    
    //MARK: Properties
    var colour: Colour!
    var tagList = [Tag]()
    var isInSaveList: Bool!
    let love = "FF5243"
    
    //MARK: IBOutlets
    @IBOutlet weak var tags: UILabel!
    @IBOutlet weak var colorCode: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    //MARK: IBActions
    @IBAction func shareBtn(_ sender: UIButton) {
        let activityViewController = UIActivityViewController(activityItems: ["0x\(colour.hex!)"], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissSwipe(_ sender: UISwipeGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func showRelated(_ sender: UISwipeGestureRecognizer) {
        //to-do
    }
   
    //MARK: General
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        guard colour != nil else { return }
        if let selectedColor = colour.hex {
            view.backgroundColor = UIColor.init(hex: selectedColor)
            colorCode.text = "0x\(colour.hex!)"
            fetchData(colour.hex)
        }
        
        saveBtn.addTarget(self, action: #selector(save), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if Utility.isSignedIn() {
           isExist(colour.hex)
        }
        
    }
    
    func fetchData(_ color: String) {
        let ws = WSColorDetail(color)
        ws.initiateRequest(success: { (result) in
            DispatchQueue.main.async {
                if let list = result as? [Tag] {
                    self.tagList = list
                    var text: String = self.tags.text!
                    if list.count > 0 {
                        var counter: Int = 0
                        let limit: Int = 22
                        for tag in list {
                            text += " \(tag.name!),"
                            counter += 1
                            if counter == limit {
                                break
                            }
                        }
                        text = String(text.dropLast())
                        if counter == limit {
                            text += "..."
                        }
                    } else {
                        text += " No tags found"
                    }
                    self.tags.text = text
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
}

extension ColorDetailViewController {
    
    @objc func save() {
        if Utility.isSignedIn() {
            guard isInSaveList != nil else { return }
            if !isInSaveList {
                let ws = WSSave(true)
                if ws.addToSave(colour.hex!) {
                    saveBtn.tintColor = UIColor.init(hex: love)
                }
            } else {
                unSave()
            }
        } else {
            self.performSegue(withIdentifier: segueIdentifier, sender: self)
        }
    }
    
    func isExist(_ input: String) {
        let ws = WSFirebase(true)
        let ref = ws.ref
        ref!.observeSingleEvent(of: .value, with: { (snapshot) in
            self.isInSaveList = false
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let savedItem = item.value as? [String: Any] {
                    let hex = savedItem["hex"] as! String
                    if hex == input {
                        self.isInSaveList = true
                        break
                    }
                }
            }
            if !self.isInSaveList {
                self.saveBtn.tintColor = .white
            } else {
                self.saveBtn.tintColor = UIColor.init(hex: self.love)
            }
        })
    }
    
    func unSave() {
        let databaseManager = WSSave(true)
        let ref = databaseManager.getDatabaseRef()
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let savedItem = item.value as? [String: AnyObject] {
                    for (key, item) in savedItem {
                        if let saveDetail = item as? [String: Any] {
                            let hex = saveDetail["hex"] as? String
                            if self.colour.hex == hex {
                                if databaseManager.removeFromSave(key) {
                                    self.isInSaveList = false
                                    self.saveBtn.tintColor = .white
                                }
                            }
                        }
                    }
                }
            }
        })
    }
}
