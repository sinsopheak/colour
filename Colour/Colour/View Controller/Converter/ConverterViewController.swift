//
//  ConverterViewController.swift
//  Colour
//
//  Created by gis on 9/26/18.
//

import UIKit

private var segueIdentifier = "viewDetail"

class ConverterViewController: UIViewController {
    
    // MARK: Properties
    var isHex: Bool = true
    var defaultTextColor: String = "FFFFFF"
    var defaultBG: String = "464646"
    var hexBG: String!
    var rgbBG: String!
    var color: Colour!
    var contrastColor: Colour!
    var isResultBeingDisplayed = [false, false] {
        didSet {
            if isHex {
                more.isEnabled = isResultBeingDisplayed[0]
            } else {
                more.isEnabled = isResultBeingDisplayed[1]
            }
        }
    }

    // MARK: IBOutlet
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var valueBox: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var hexView: UIStackView!
    
    @IBOutlet weak var rgbView: UIStackView!
    @IBOutlet weak var rField: UITextField!
    @IBOutlet weak var gField: UITextField!
    @IBOutlet weak var bField: UITextField!
    @IBOutlet weak var rgbInfo: UILabel!
    @IBOutlet weak var more: UIBarButtonItem!
    
    // MARK: IBAction
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @IBAction func segmentChange(_ sender: Any) {
        switch segmentedController.selectedSegmentIndex {
        case 0:
            toggleView(true)
            view.backgroundColor = UIColor.init(hex: hexBG!)
            more.isEnabled = isResultBeingDisplayed[0]
        case 1:
            toggleView(false)
            view.backgroundColor = UIColor.init(hex: rgbBG!)
            more.isEnabled = isResultBeingDisplayed[1]
        default:
            break
        }
    }
    
    @IBAction func reset(_ sender: UIBarButtonItem) {
        if isHex {
            isResultBeingDisplayed[0] = false
            valueBox.text = ""
            hexBG = defaultBG
        } else {
            isResultBeingDisplayed[1] = false
            rgbBG = defaultBG
            gField.text = ""
            rField.text = ""
            bField.text = ""
        }
        view.backgroundColor = UIColor.init(hex: defaultBG)
        view.endEditing(true)
        self.updateUI(getDefaultText())
    }
    
    // MARK: General
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        valueBox.delegate = self
        rField.delegate = self
        gField.delegate = self
        bField.delegate = self
        hexBG = defaultBG
        rgbBG = defaultBG
        
        more.target = self
        more.action = #selector(moreAction)
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let destination = segue.destination as? ColorDetailViewController
            destination?.colour = color
        }
    }

}

extension ConverterViewController {
    @objc func moreAction() {
        let actionSheet = UIAlertController(title: "More", message: nil, preferredStyle: .actionSheet)
        let detailButon = UIAlertAction(title: "Color Detail", style: .default, handler: { action in
            self.viewDetail()
        })
        let contrastButton = UIAlertAction(title: "Contrast Color", style: .default, handler: { action in
            self.viewContrast()
        })
        let shareButton = UIAlertAction(title: "Share", style: .default) { (action) in
            self.share()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(detailButon)
        actionSheet.addAction(contrastButton)
        actionSheet.addAction(shareButton)
        actionSheet.addAction(cancelButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func share() {
        var shareText: String!
        if isHex {
            shareText = resultLabel.text
        } else {
            shareText = rgbInfo.text
        }
        let activityViewController = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    
    func viewDetail() {
        var value: String!
        if isHex {
            value = valueBox.text
        } else {
            value = String((rgbInfo.text?.dropFirst())!)
        }
        color = Colour(0, value)
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    func viewContrast() {
        color = contrastColor
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
}

extension ConverterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //to-do
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.count == 0 {
            if isHex {
                if textField.text?.count == 1 {
                    //text field is empty
                    //restore default value
                    view.backgroundColor = UIColor.init(hex: defaultBG)
                    isResultBeingDisplayed[0] = false
                    updateUI(getDefaultText())
                }
            } else {
                let tag = getTag(textField)
                if tag >= 0 {
                    let value = String((textField.text?.dropLast())!)
                    getRGBvalues(tag, value)
                }
            }
            return true
        }
        let maxCharacters = isHex ? 6 : 3
        let stringType = isHex ? Constant.hexString : Constant.rgbString
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        var isCorrect: Bool = false
        if prospectiveText.count <= maxCharacters && prospectiveText.containsOnlyCharactersIn(stringType) {
            isCorrect = true
            let value = textField.text! + string
            if isHex {
                if prospectiveText.count == maxCharacters {
                    view.backgroundColor = UIColor.init(hex: value)
                    hexBG = value
                    fetchData([value])
                }
            } else {
                let totalValue = (value as NSString).integerValue
                if totalValue > 255 {
                    isCorrect = false
                } else {
                    let tag = getTag(textField)
                    if tag >= 0 {
                        getRGBvalues(tag, value)
                    }
                }
            }
           
        }
        return isCorrect
        
    }
    
    func getTag(_ textField: UITextField) -> Int {
        switch textField {
        case rField:
            return 0
        case gField:
            return 1
        case bField:
            return 2
        default: return -1
        }
    }
    
    func getRGBvalues(_ tag: Int, _ text: String) {
        guard !text.isEmpty else { return }
        switch tag {
        case 0:
            guard !(gField.text?.isEmpty)!, !(bField.text?.isEmpty)! else { return }
        case 1:
            guard !(rField.text?.isEmpty)!, !(bField.text?.isEmpty)! else { return }
        case 2:
            guard !(rField.text?.isEmpty)!, !(gField.text?.isEmpty)! else { return }
        default:
            return
        }
        var values = [rField.text!, gField.text!, bField.text!]
        values[tag] = text
        fetchData(values)
    }
    
}

extension ConverterViewController {
    func fetchData(_ value: [String]) {
        if !isHex {
            guard value.count == 3 else { return }
            rgbInfo.text = "Converting..."
        } else {
            resultLabel.text = "Converting..."
        }
        let ws = WSConvert(value, isHex)
        ws.initiateRequest(success: { (result) in
            DispatchQueue.main.async {
                if let response = result as? [String] {
                    guard response.count == 2 else { return }
                    var contrastValue = response[1]
                    contrastValue = String(contrastValue.dropFirst())
                    self.updateUI(response[0])
                    if !self.isHex {
                        self.isResultBeingDisplayed[1] = true
                        self.view.backgroundColor = UIColor.init(hex: response[0])
                        self.rgbBG = response[0]
                    } else {
                        self.isResultBeingDisplayed[0] = true
                        self.view.endEditing(true)
                    }
                    self.contrastColor = Colour(0, contrastValue)
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    func updateUI(_ text: String!) {
        if isHex {
            resultLabel.text = text
        } else {
            rgbInfo.text = "#" + text
        }
    }
    
    func toggleView(_ isHexView: Bool) {
        isHex = isHexView
        valueBox.isHidden = !isHex
        hexView.isHidden = !isHex
        rgbView.isHidden = isHex
    }
    
    func getDefaultText() -> String {
        if isHex {
            return "Hex to RGB"
        }
        return "RGB to Hex"
    }
}
