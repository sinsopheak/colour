//
//  GeneralTableViewController.swift
//  Colour
//
//  Created by gis on 9/26/18.
//

import UIKit

class GeneralTableViewController: UITableViewController {
    
    // MARK: Properties
    
    var segueIdentifier = ""
    let threshold: CGFloat = 100.0 // threshold from bottom of tableView
    var isLoadingMore = false
    var selectedIndex: Int!
    var isHome: Bool = true
    var searchController: UISearchController!
    var isFirstView: Bool = true
    
    // MARK: General
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(goTop), name: Notification.Name.goTop, object: nil)
        
        searchController = UISearchController(searchResultsController: nil)
  
        if #available(iOS 11.0, *) {
            if isHome {
                searchController.obscuresBackgroundDuringPresentation = false
                searchController.searchBar.placeholder = "Search"
                searchController.searchResultsUpdater = self
                searchController.searchBar.tintColor = .white
                navigationItem.searchController = searchController
                definesPresentationContext = true
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isHome && !isSearching() {
            if !isFirstView {
                let contentOffset = scrollView.contentOffset.y
                let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
                
                if !isLoadingMore && (maximumOffset - contentOffset <= threshold) {
                    self.isLoadingMore = true
                    fetchData()
                }
            } else {
                isFirstView = false
            }
        }
    }
    
    func fetchData() {}
    func filterContentForSearch(_ searchText: String) {}
    
    @objc func refresh() {
        fetchData()
    }
    func endRefreshing() {}
    
    @objc func goTop() {
        tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
    }
    
}

extension GeneralTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearch(searchController.searchBar.text!)
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isSearching() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
}
