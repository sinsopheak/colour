//
//  HomeTableViewController.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import UIKit

private let reuseIdentifier = "homeCell"

class HomeTableViewController: GeneralTableViewController {
    
    // MARK: IBOutlet
    @IBOutlet weak var refreshController: UIRefreshControl!
    
    // MARK: IBAction
    @IBAction func colorPicker(_ sender: UIBarButtonItem) {
        //to-do
    }
    // MARK: Properties
    var data = [Colour]()
    var searchResult = [Colour]()
    
    // MARK: General
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segueIdentifier = "colorDetail"
        
        if isHome {
            refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
            fetchData()
            self.navigationItem.rightBarButtonItem = nil
        } else {
            self.refreshControl = nil
            if #available(iOS 11.0, *) {
                self.navigationController?.navigationBar.prefersLargeTitles = false
            }
            let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.action, target: self, action: #selector(share))
            self.navigationItem.rightBarButtonItem = button
        }
        
    }
    
    @objc func share() {
        var shareText: String = "Scheme colours: "
        for color in data {
            let colorText = "#" + color.hex! + ", "
            shareText += colorText
        }
        let index = shareText.index(shareText.endIndex, offsetBy: -2)
        shareText = String(shareText[..<index])
        let activityViewController = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearching() {
            return searchResult.count
        }
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! HomeTableViewCell
        
        // Configure the cell...
        if isSearching() {
            if let colour = searchResult[indexPath.row].hex {
                cell.backgroundColor = UIColor.init(hex: colour)
                cell.colorCode.text = "0x" + colour
            }
        } else {
            if let colour = data[indexPath.row].hex {
                cell.backgroundColor = UIColor.init(hex: colour)
                cell.colorCode.text = "0x" + colour
            }
        }
        
        return cell
    }
    
    override func endRefreshing() {
        if (refreshController.isRefreshing) {
            refreshController.endRefreshing()
        }
        tableView.reloadData()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        guard selectedIndex != nil else { return }
        if segue.identifier == segueIdentifier {
            let destination = segue.destination as! ColorDetailViewController
            if isSearching() {
                destination.colour = searchResult[selectedIndex]
            } else {
                destination.colour = data[selectedIndex]
            }
            
        }
    }
    
    override func filterContentForSearch(_ searchText: String) {
        let ws = WSsearch(searchText, true)
        ws.initiateRequest(success: { (result) in
            DispatchQueue.main.async {
                if let response = result as? [Colour] {
                    self.searchResult = response
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    override func fetchData() {
        let ws = WSRandomColour()
        ws.initiateRequest(success: { (response) in
            if let list = response as? [Colour] {
                DispatchQueue.main.async {
                    if self.isLoadingMore {
                        for colour in list {
                            self.data.append(colour)
                        }
                        self.isLoadingMore = false
                    } else {
                        self.data = list
                    }
                    self.endRefreshing()
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
}
