//
//  SavedTableViewController.swift
//  Colour
//
//  Created by gis on 9/25/18.
//

import UIKit
import FirebaseDatabase

private let reuseIdentifier = "savedCell"
private let segueIdentifier = "colorDetail"
private let signinSegueIdentifier = "signin"

class SavedTableViewController: UITableViewController {
    
    // MARK: Properties
    var isIn: Bool = false
    var saves = [Saved]()
    var selectedColorIndex: Int!
    var ref: DatabaseReference!
    var isObserving: Bool = false
    var handle: UInt = 0

    // MARK: General

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        
        let searchController = UISearchController(searchResultsController: nil)
        if #available(iOS 11.0, *) {
            searchController.obscuresBackgroundDuringPresentation = false
            searchController.searchBar.placeholder = "Search"
            navigationItem.searchController = searchController
            definesPresentationContext = true
        }
        
        if Utility.isSignedIn() {
            fetchData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (Utility.isSignedIn() && !isObserving) {
            fetchData()
        } else if !Utility.isSignedIn() && isObserving {
            clear()
        }
        
        if !Utility.isSignedIn() {
            if !self.isIn {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.isIn = true
                    self.performSegue(withIdentifier: signinSegueIdentifier, sender: self)
                })
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return saves.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! HomeTableViewCell
        
        // Configure the cell...
        if let colour = saves[indexPath.row].colour {
            if let hex = colour.hex {
                cell.backgroundColor = UIColor.init(hex: hex)
                cell.colorCode.text = "0x" + hex
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedColorIndex = indexPath.row
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
 
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
     // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let ws = WSSave(true)
            let id = saves[indexPath.row].id
            if ws.removeFromSave(id) {
                saves.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        guard selectedColorIndex != nil else { return }
        if segue.identifier == segueIdentifier {
            let destination = segue.destination as! ColorDetailViewController
            destination.colour = saves[selectedColorIndex].colour
        } else if segue.identifier == signinSegueIdentifier {
            let destination = segue.destination as! SigninViewController
            destination.isFromSave = true
        }
    }

}

extension SavedTableViewController {
    
    private func clear() {
        isObserving = false
        saves.removeAll()
        ref.removeObserver(withHandle: handle)
        tableView.reloadData()
    }
    
    private func fetchData() {
        let databaseManager = WSSave(true)
        ref = databaseManager.getDatabaseRef()
        handle = ref.observe(DataEventType.value, with: { (snapshot) in
            self.isObserving = true
            if snapshot.childrenCount > 0 {
                self.saves.removeAll()
                
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    if let savedItem = item.value as? [String: AnyObject] {
                        for (key, item) in savedItem {
                            if let saveDetail = item as? [String: Any] {
                                let hex = saveDetail["hex"] as? String
                                let date = saveDetail["dateAdded"] as? String
                                let color = Colour(0, hex)
                                let save = Saved(key, color, date)
                                self.saves.append(save)
                            }
                        }
                    }
                }
                self.tableView.reloadData()
            }
        })
    }
}
