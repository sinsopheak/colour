//
//  SigninViewController.swift
//  Colour
//
//  Created by gis on 9/27/18.
//

import UIKit
import FirebaseAuth

class SigninViewController: UIViewController {
    
    // MARK: Properties
    var isSignin: Bool = true
    var isFromSave: Bool = true
    
    // MARK: IBOutlets
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var info: UIButton!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: IBActions
    
    @IBAction func tapToDismiss(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func swipeToDismiss(_ sender: UISwipeGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: General
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        info.addTarget(self, action: #selector(switchForm), for: .touchUpInside)
        actionBtn.addTarget(self, action: #selector(processForm), for: .touchUpInside)
        if !isFromSave {
            topLabel.text = "Colour."
            topLabel.font = UIFont.boldSystemFont(ofSize: 35)
            topLabel.textColor = UIColor.init(hex: "40B4EA")
        }
    }
    
    @objc func switchForm() {
        resetUI()
        isSignin = !isSignin
        if isSignin {
            info.setTitle("Don't have account? Create one.", for: .normal)
            actionBtn.setTitle("Sign in", for: .normal)
        } else {
            info.setTitle("Already have account? Sign in.", for: .normal)
            actionBtn.setTitle("Create account", for: .normal)
        }
        confirmPassword.isHidden = isSignin
        if isFromSave {
            topLabel.isHidden = !isSignin
        }
    }
    
    @objc func processForm() {
        resetUI()
        var isIncomplete: Bool = false
        if (username.text?.isEmpty)! {
            updateUI(username, true)
            isIncomplete = true
        }
        if (password.text?.isEmpty)! {
            updateUI(password, true)
            isIncomplete = true
        }
        if !isIncomplete || !isSignin {
            activityIndicator.startAnimating()
            if isSignin {
               signin()
            } else {
                if (confirmPassword.text?.isEmpty)! || (confirmPassword.text != password.text) {
                    updateUI(confirmPassword, true)
                    return
                }
                createAccount()
            }
        }
        
    }
    
    func updateUI(_ textField: UITextField, _ isWrong: Bool) {
        if isWrong {
            textField.layer.borderWidth = 2.0
            textField.layer.cornerRadius = 5.0
            textField.layer.borderColor = UIColor.init(hex: "FF7B62").cgColor
        } else {
            textField.layer.borderWidth = 0.0
        }
    }
    
    func resetUI() {
        errorLabel.isHidden = true
        updateUI(username, false)
        updateUI(password, false)
        updateUI(confirmPassword, false)
    }
    
    func addUserStateListener() {
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                print("signed in")
            }
        }
    }
    
    // MARK: Account delegates
    
    func signin() {
        let ws = WSAccount()
        ws.processForm(["email": username.text!, "password": password.text!, "isSignIn": true], success: { (response) in
            self.completeSignin(response)
        }) { (error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func createAccount() {
        let ws = WSAccount()
        ws.processForm(["email": username.text!, "password": password.text!, "isSignIn": false], success: { (response) in
            self.completeSignin(response)
        }) { (error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
        
    }
    
    func completeSignin(_ response: Any) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            if let isSucceed = response as? Bool {
                if isSucceed {
                    Utility.setAccountState(true)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.errorLabel.text = "Please try again"
                    self.errorLabel.isHidden = false
                }
            } else {
                self.errorLabel.text = response as? String
                self.errorLabel.isHidden = false
            }
        }
       
    }

}
