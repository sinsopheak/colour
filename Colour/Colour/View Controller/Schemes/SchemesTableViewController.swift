//
//  SchemesTableViewController.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import UIKit

private var reuseIdentifier = "schemeCell"
private var collectionViewIdentifier = "schemeCollectionCell"

class SchemesTableViewController: GeneralTableViewController {

    // MARK: Property
    var schemes = [Scheme]()
    var searchResult = [Scheme]()
    
    // MARK: IBOutlet
    @IBOutlet weak var refreshController: UIRefreshControl!
    
    // MARK: General
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segueIdentifier = "showSchemeDetail"
        refreshController.addTarget(self, action: #selector(refresh), for: .valueChanged)
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            if !(self.navigationController?.navigationBar.prefersLargeTitles)! {
                self.navigationController?.navigationBar.prefersLargeTitles = true
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearching() {
            return searchResult.count
        }
        return schemes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        // Configure the cell...
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? SchemeTableViewCell else { return }
     
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let destination = segue.destination as! HomeTableViewController
            if isSearching() {
                destination.data = searchResult[selectedIndex].colors
            } else {
                destination.data = schemes[selectedIndex].colors
            }
            destination.isHome = false
        }
    }
    
    override func fetchData() {
        let ws = WSRandomScheme()
        ws.initiateRequest(success: { (result) in
            DispatchQueue.main.async {
                if let response = result as? [Scheme] {
                    if self.isLoadingMore {
                        for scheme in response {
                            self.schemes.append(scheme)
                        }
                        self.isLoadingMore = false
                    } else {
                        self.schemes = response
                    }
                    self.endRefreshing()
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    override func filterContentForSearch(_ searchText: String) {
        let ws = WSsearch(searchText, false)
        ws.initiateRequest(success: { (result) in
            DispatchQueue.main.async {
                if let response = result as? [Scheme] {
                    self.searchResult = response
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    override func endRefreshing() {
        if (refreshController.isRefreshing) {
            refreshController.endRefreshing()
        }
        tableView.reloadData()
    }

}

extension SchemesTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching() {
            return searchResult[collectionView.tag].colors.count
        }
        return schemes[collectionView.tag].colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewIdentifier, for: indexPath)
    
        var color: Colour!
        var scheme: Scheme!
        if isSearching() {
            scheme = searchResult[collectionView.tag]
        } else {
            scheme = schemes[collectionView.tag]
        }
        color = scheme.colors[indexPath.row]
        if let colourHex = color.hex {
            cell.backgroundColor = UIColor.init(hex: colourHex)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = collectionView.tag
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }

}
