//
//  AccountTableViewController.swift
//  Colour
//
//  Created by gis on 9/28/18.
//

import UIKit
import FirebaseAuth

private var reuseIdentifier = "settingCell"
private let signinSegueIdentifier = "signinFromSetting"

class SettingTableViewController: UITableViewController {
    
    // MARK: Properties
    var email: String = ""
    var isVerified: Bool = false
    var isSignedIn: Bool = false
    var sections = ["Account", "Others"]
    var data = [["Email", "Password", "Sign out"], ["About", "Terms of Use", "Report"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isSignedIn = Utility.isSignedIn()
        if isSignedIn {
            getUserInfo()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isSignedIn != Utility.isSignedIn() {
            refresh()
        }
    }
    
    func refresh() {
        isSignedIn = Utility.isSignedIn()
        if isSignedIn {
            getUserInfo()
        } else {
            email = ""
            isVerified = false
        }
        tableView.reloadSections([0], with: .automatic)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            if isSignedIn {
                return data[section].count
            }
            return 1
        }
        return data[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        // Configure the cell...
        if indexPath.section == 0 {
            if !isSignedIn {
                cell.textLabel?.text = "Sign In"
                cell.tag = 1
                cell.detailTextLabel?.text = ""
            } else {
                let text = data[indexPath.section][indexPath.row]
                cell.textLabel?.text = text
                if indexPath.row == 0 {
                    cell.detailTextLabel?.text = email
                } else {
                    cell.detailTextLabel?.text = ""
                    if indexPath.row == 2 {
                        cell.tag = 2
                    }
                }
            }
        } else {
            let text = data[indexPath.section][indexPath.row]
            cell.textLabel?.text = text
            cell.detailTextLabel?.text = ""
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        switch cell?.tag {
        case 1:
            //to-do signin
            self.performSegue(withIdentifier: signinSegueIdentifier, sender: self)
            refresh()
        case 2:
            //to-do signout
            try! Auth.auth().signOut()
            Utility.setAccountState(false)
            refresh()
        default:
            return
        }
    }
    
    func getUserInfo() {
        if Auth.auth().currentUser != nil {
            email = (Auth.auth().currentUser?.email)!
            isVerified = (Auth.auth().currentUser?.isEmailVerified)!
        }
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == signinSegueIdentifier {
            let destination = segue.destination as! SigninViewController
            destination.isFromSave = false
        }
    }

}
