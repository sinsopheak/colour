//
//  TabbarViewController.swift
//  Colour
//
//  Created by gis on 9/27/18.
//

import UIKit

class TabbarViewController: UITabBarController {
    var lastSelected: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if tabBar.tag == lastSelected {
            NotificationCenter.default.post(name: Notification.Name.goTop, object: nil)
        } else {
            lastSelected = tabBar.tag
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
