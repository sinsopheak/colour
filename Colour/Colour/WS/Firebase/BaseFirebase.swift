//
//  BaseFirebase.swift
//  Colour
//
//  Created by gis on 9/28/18.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class WSFirebase {
    
    var ref: DatabaseReference!
    var isColour: Bool!
    var route: String!
    
    init(_ isColour: Bool) {
        self.isColour = isColour
        setRoute()
        ref = Database.database().reference().child("saves").child(getUserID()).child(route)
    }
    
    func getUserID() -> String {
        return (Auth.auth().currentUser?.uid)!
    }
    
    func setRoute() {
        if isColour {
            route = "colours"
        } else {
            route = "schemes"
        }
    }
    
    func getDatabaseRef() -> DatabaseReference {
        return Database.database().reference().child("saves").child(getUserID())
    }
    
}
