//
//  WSAccount.swift
//  Colour
//
//  Created by gis on 9/27/18.
//

import Foundation
import FirebaseAuth

class WSAccount {
    
    func processForm(_ data: [String: Any], success:((_ id:Any)->Void)? , err:((_ err:String)->Void)?) {
        do {
            try self.accountRequest(data, success: success, err: err)
        } catch let e as NSError {
            if err != nil {
                err!(e.localizedDescription)
            }
        }
    }
    
    private func accountRequest(_ data: [String: Any], success:((_ id:Any)->Void)? , err:((_ err:String)->Void)?) throws {
        let email = data["email"] as? String
        let password = data["password"] as? String
        if let isSignIn = data["isSignIn"] as? Bool {
            if isSignIn {
                self.signin(email!, password!, success: success, err: err)
            } else {
                self.createAccount(email!, password!, success: success, err: err)
            }
        }
    }
    
    private func signin(_ email: String, _ password: String, success:((_ id:Any)->Void)? , err:((_ err:String)->Void)?) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    success!(self.getErrorMsg(errCode))
                    return
                }
                success!(false)
            } else {
                success!(true)
            }
        }
    }
    
    private func createAccount(_ email: String, _ password: String, success:((_ id:Any)->Void)? , err:((_ err:String)->Void)?) {
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            // ...
            guard (authResult?.user) != nil else {
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    success!(self.getErrorMsg(errCode))
                    return
                }
                success!(false)
                return
            }
            Auth.auth().currentUser?.sendEmailVerification { (error) in
                if error != nil {
                    success!(false)
                    return
                }
                success!(true)
            }
        }
    }
    
    private func getErrorMsg(_ errCode: AuthErrorCode) -> String {
        switch errCode {
        case .emailAlreadyInUse:
            return "Account already exists"
        case .invalidEmail:
            return "Invalid email address"
        case .wrongPassword, .userNotFound:
            return "Incorrect password or email"
        case  .weakPassword:
            return "Passwords must be at least 6 characters"
        case .networkError:
            return "Connection failed"
        default:
            return "There was problem with the server"
        }
    }
        
    
}
