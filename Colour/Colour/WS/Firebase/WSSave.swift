//
//  WSSave.swift
//  Colour
//
//  Created by gis on 9/28/18.
//

import Foundation
//import FirebaseDatabase
//import FirebaseAuth

class WSSave: WSFirebase {
    
    func addToSave(_ hex: String) -> Bool {
        if !Utility.isSignedIn() {
            return false
        }
        let saveItem = ["hex": hex, "dateAdded": Utility.getCurrentDateTime()]
        let key = ref.childByAutoId().key
        ref.child(key!).setValue(saveItem)
        return true
    }
    
    func removeFromSave(_ id: String) -> Bool {
        ref.child(id).setValue(nil)
        return true
    }
    
}
