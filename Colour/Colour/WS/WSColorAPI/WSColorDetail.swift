//
//  WSColorDetail.swift
//  Colour
//
//  Created by gis on 9/25/18.
//

import Foundation

class WSColorDetail: WebService {
    
    var color: String!
    
    override init() {
        super.init()
    }
    
    init(_ color: String!) {
        self.color = color
    }
    
    override func getURL() -> String {
        return Constant.colorDetail + color
    }
    
    override func decodeData(data: [String : Any]) -> [Any] {
        var result = [Tag]()
        if let colour = data["colors"] as? [Any] {
            if let colourData = colour[0] as? [String: Any] {
                if let tags = colourData["tags"] as? [Any] {
                    for tag in tags {
                        if let tagObj = tag as? [String: Any] {
                            let id = tagObj["id"] as? Int
                            let name = tagObj["name"] as? String
                            let newTag = Tag(id, name)
                            result.append(newTag)
                        }
                    }
                }
            }
        }
        return result
    }
    
}
