//
//  WSConvert.swift
//  Colour
//
//  Created by gis on 9/26/18.
//

import Foundation

class WSConvert: WebService {
    
    var isConvertToRGB: Bool!
    var value = [String]()
    
    override func isConvertAPI() -> Bool {
        return true
    }
    
    override func getURL() -> String {
        if isConvertToRGB {
            return Constant.convertHexValue + value[0]
        }
        return Constant.convertRGBValue + formatStringToRGB(value)
    }
    
    func formatStringToRGB(_ rgbTexts: [String]) -> String {
        return "(" + rgbTexts[0] + "," + rgbTexts[1] + "," + rgbTexts[2] + ")"
    }
    
    override init() {
        super.init()
    }
    
    init(_ value: [String], _ isConvertToRGB: Bool!) {
        self.value = value
        self.isConvertToRGB = isConvertToRGB
    }
    
    override func decodeData(data: [String : Any]) -> [Any] {
        if isConvertToRGB {
           return getRGB(data)
        }
        return getHex(data)
    }
    
    func getRGB(_ data: [String: Any]) -> [String] {
        var response = [String]()
        if let rgb = data["rgb"] as? [String: Any] {
            if let value = rgb["value"] as? String {
                response.append(value)
            }
            response.append(getContrast(data))
        }
        return response
    }
    
    func getHex(_ data: [String: Any]) -> [String] {
        var response = [String]()
        if let hex = data["hex"] as? [String: Any] {
            if let value = hex["clean"] as? String {
                response.append(value)
            }
        }
        response.append(getContrast(data))
        return response
    }
    
    func getContrast(_ data: [String: Any]) -> String {
        var contrastString = ""
        if let contrast = data["contrast"] as? [String: Any] {
            if let colorValue = contrast["value"] as? String {
                contrastString = colorValue
            }
        }
        return contrastString
    }
}
