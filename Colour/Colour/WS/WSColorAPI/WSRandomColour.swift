//
//  WSRandomColour.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import Foundation

class WSRandomColour: WebService {
    override func getURL() -> String {
        return Constant.randomColorURL + "20"
    }
    
    override func decodeData(data: [String : Any]) -> [Any] {
        return decodeColor(data)
    }
}
