//
//  WSRandomScheme.swift
//  Colour
//
//  Created by gis on 9/25/18.
//

import Foundation

class WSRandomScheme: WebService {
    override func getURL() -> String {
        return Constant.randomSchemeURL + "20"
    }
    
    override func decodeData(data: [String : Any]) -> [Any] {
        return decodeScheme(data)
    }
}
