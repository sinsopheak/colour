//
//  WSsearch.swift
//  Colour
//
//  Created by gis on 9/26/18.
//

import Foundation

class WSsearch: WebService {
    var keyword: String!
    var isSearchColor: Bool!
    override func getURL() -> String {
        return Constant.searchURL + keyword
    }
    
    override init() {
        super.init()
    }
    
    init(_ keyword: String, _ isSearchColor: Bool) {
        self.keyword = keyword
        self.isSearchColor = isSearchColor
    }
    
    override func decodeData(data: [String : Any]) -> [Any] {
        if isSearchColor {
            return decodeColor(data)
        }
        return decodeScheme(data)
    }
    
}
