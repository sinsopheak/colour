//
//  WebService.swift
//  Colour
//
//  Created by gis on 9/24/18.
//

import Foundation

class WebService: NSObject , URLSessionDelegate {
    
    var data = [String: Any]()
    var url: String!
    
    enum Method: String {
        case GET
        case POST
    }
    
    func isConvertAPI() -> Bool {
        return false
    }
    
    func getURL() -> String {
        return ""
    }
    
    func getMethod() -> String {
        return Method.GET.rawValue
    }
    
    func processData() {
        //to-do
    }
    
    func initiateRequest(success:((_ id:Any)->Void)? , error:((_ error:String)->Void)?) {
        do {
            try self.sendRequest(success: success, error: error)
        } catch let e as NSError {
            
            if error != nil {
                error!(e.localizedDescription)
            }
            
        }
    }
    
    private func sendRequest(success:((_ id:Any)->Void)? , error:((_ error:String)->Void)?) throws {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        url = getURL()
        let urlRequest = NSMutableURLRequest(url: URL(string: url)!)
        if !data.isEmpty {
            processData()
            if self.getMethod() == Method.POST.rawValue {
                let _data = try JSONSerialization.data(withJSONObject: data, options: .init(rawValue: 0))
                let stringJson = String(data: _data, encoding: String.Encoding.utf8)
                urlRequest.httpBody = stringJson?.data(using: String.Encoding.utf8)
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        }
        urlRequest.url = URL(string: url)
        
        session.dataTask(with: urlRequest as URLRequest) { (d,res,err) in
            if let err = err {
                error!((err.localizedDescription))
            } else {
                do {
                    
                    let response = try JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    if let data = response as? [String: Any] {
                        if !self.isConvertAPI() {
                            if let isSucceed = data["success"] as? Bool {
                                if isSucceed {
                                    success!(self.decodeData(data: data))
                                }
                                success!([:])
                            }
                        } else {
                            success!(self.decodeData(data: data))
                        }
                        
                    }
                    
                } catch let e as NSError {
                    error!(e.localizedDescription)
                }
            }
            
            }.resume()
    }
    
    func decodeData(data: [String: Any]) -> [Any] {
        return []
    }
    
    func decodeColor(_ data: [String: Any]) -> [Colour] {
        var result = [Colour]()
        if let colours = data["colors"] as? [Any] {
            for colour in colours {
                if let colourData = colour as? [String: Any] {
                    var hex = colourData["hex"] as? String
                    if (hex?.isEmpty)! {
                        hex = "000000"
                    }
                    let id = colourData["id"] as? Int
                    let colourObj = Colour(id, hex)
                    result.append(colourObj)
                }
            }
        }
        return result
    }
    
    func decodeScheme(_ data: [String: Any]) -> [Scheme] {
        var schemeList = [Scheme]()
        if let schemes = data["schemes"] as? [Any] {
            for scheme in schemes {
                if let schemeObj = scheme as? [String: Any] {
                    var colorList = [Colour]()
                    if let colors = schemeObj["colors"] as? [String] {
                        if colors.count > 2 {
                            for colorHex in colors {
                                var hex = colorHex
                                if hex.isEmpty {
                                    hex = "000000"
                                }
                                let color = Colour(0, hex)
                                colorList.append(color)
                            }
                        } else {
                            continue
                        }
                    }
                    let idString = schemeObj["id"] as? String
                    let idInt = (idString! as NSString).integerValue
                    var tagList = [Tag]()
                    if let tags = schemeObj["tags"] as? [Any] {
                        for tagObj in tags {
                            if let tagObject = tagObj as? [String: Any] {
                                let tagId = tagObject["id"] as? String
                                let tagIdInt = (tagId! as NSString).integerValue
                                let name = tagObject["name"] as? String
                                let tag = Tag(tagIdInt, name)
                                tagList.append(tag)
                            }
                        }
                    }
                    let newScheme = Scheme(idInt, colorList, tagList)
                    schemeList.append(newScheme)
                }
            }
        }
        return schemeList
    }
    
}
